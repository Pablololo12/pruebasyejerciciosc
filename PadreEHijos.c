//Este programa/prueba, se ejecuta junto a un argumento que indica el número de veces que el proceso padre tiene que escribir unos
//    y el numero de veces que dos hijos tienen que escribir 2s y 3s


#include<stdlib.h>
#include<unistd.h>

int main(int argc, char *argv[]){
        //con esto leemos el argumento, si es distinto a uno muestra un error
        int n;
        if(argc!=2){
                write(2,"Error",5); exit(-1);}
        else{
                n=atoi(argv[1]);}
        
        
        //con el for ejecutamos los dos hijos
        for(int i=2;i!=4;i++){
                //Se ejecutan los hijos despues de for manteniendo asi el entero i que indica el numero de hijo
                int d=fork();
                
                switch(d){
                //Si se falla al crear los hijos Muestra un error y termina el programa
                case -1:
                        write(2,"Error",5);
                        exit(-1);
                //sino dependiendo del numero de hijo se escribe una cosa u otra
                case 0:
                        if(i==2)
                        for(int t=0; t!=n; t++){
                                write(2,"2",1);
                        }
                        else
                        for(int t=0; t!=n; t++){
                                write(2,"3",1);
                        }
                        exit(0);
                default:
                        break;
                }
        }
        //el programa padre escribe por pantalla después de invocar a los hijos
        for(int t=0; t!=n; t++){
                write(2,"1",1);
        }
        return(0);
}
